.PHONY: clean All

All:
	@echo ----------Building project:[ bkdown - Debug ]----------
	@cd "bkdown" && "$(MAKE)" -f "bkdown.mk"
clean:
	@echo ----------Cleaning project:[ bkdown - Debug ]----------
	@cd "bkdown" && "$(MAKE)" -f "bkdown.mk" clean
