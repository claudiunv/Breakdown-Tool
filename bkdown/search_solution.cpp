#include "search_solution.h"
#include <iostream>
#include <algorithm>

using namespace std;

SearchSolution::SearchSolution()
{
}

SearchSolution::~SearchSolution()
{
}

void SearchSolution::set_numbers( vector<long long int>& nrs)
{
	numbers.swap(nrs);
	/*
	for(int i=0; i<numbers.size(); i++)
		cout<<"adaug: "<<numbers[i]<<endl;
		 */
}

vector<long long int> SearchSolution::get_results()
{

	return( results );
}

void SearchSolution::set_result( long long int solution )
{
	final_result = solution;
}

void SearchSolution::get_solution()
{
	//for( int i=0; i<3; i++ )
	//	numbers.push_back( 7 );


	//final_result = 68605;
	//cout<<"expected: "<<final_result<<endl;

	mix_solution();
}
void SearchSolution::row_solution()
{
	unsigned long long partial_mask = 0;
	int j=0;
	long long partial_sum = 0;

	for( unsigned long long i = 1; i <= numbers.size(); i++) {
		partial_mask = i;
		j = 0;
		partial_sum = 0;
		while( partial_mask ) {
			partial_sum += numbers[j]*(partial_mask & 1);
			partial_mask >>= 1;
			j++;
		}
		//cout<<endl;
		//cout<<"sum is "<<n<<endl;
		if( partial_sum == final_result ) {
			found = 1;
			//generate results list
			partial_mask = i;
			j = 0;
			while( partial_mask ) {
				if( numbers[j]*(partial_mask & 1) )
					results.push_back( numbers[j] );
				partial_mask >>= 1;
				j++;
			}
			break;
		}
	}
}

/**
 * Separate positive numbers by negative, sort positive and negative vectors
 * add secquencial numbers from biggest number for positive and negative, if
 * is not found, add the required final sum to the positive once and take binary
 * sum order negative numbers as solution, add positive vector since we have a
 * modulo negative sum and that is the solution. take out the destination sum,
 * add the used negative numbers amd build the list to return.
 */
void SearchSolution::mix_solution()
{
	vector<long long int>positives;
	vector<long long int>negatives;
	vector<long long int>temp_neg;
	bool neg_is_empty = false;
	bool pos_is_empty = false;
	long long int partial_mask = 0;
	int j = 0;
	long long int n = 0;
	long long int partial_number = 0;

	/// Build positive and negative list
	for( int i=0; i<numbers.size(); i++ ) {
		if( numbers.at(i) >= 0 )
			positives.push_back(numbers.at(i));
	}

	for( int i=0; i<numbers.size(); i++ ) {
		if( numbers.at(i) < 0 )
			negatives.push_back(numbers.at(i));
	}

	/// Sorting vectors
	if( positives.empty() )
		pos_is_empty = true;
	else {
		sort(positives.begin(), positives.end() );
		reverse(positives.begin(), positives.end() );
	}

	if( negatives.empty())
		neg_is_empty = true;
	else {
		sort(negatives.begin(), negatives.end() );
		//reverse(negatives.begin(), negatives.end() );
	}

/*
	for( int i = 0; i< positives.size(); i++ )
		cout<<"positive number list["<<i<<"]: "<<positives.at(i)<<endl;
	for( int i = 0; i< negatives.size(); i++ )
		cout<<"negative number list["<<i<<"]: "<<negatives.at(i)<<endl;
*/

	/// Search solution only for positive numbers
	if( group_search( positives, results, final_result ) == true ) {
/*
		for( int i = 0; i< results.size(); i++ )
		{
			cout<<"results number list["<<i<<"]: "<<results.at(i)<<endl;
		}
*/
		found = true;
	} else {
		cout<<"Is not found, search with negative numbers algorithm."<<endl;

		results.clear();
		if( neg_is_empty == false ) {

			for( unsigned long long int i = 0; i < ( 1<<negatives.size() ); i++ ) {
				partial_mask = i;
				j=0;
				n = 0;
				while( partial_mask ) {
					partial_number = negatives[j]*(partial_mask & 1);
					n += partial_number * -1;
					if( partial_number ) {
						temp_neg.push_back( partial_number );
					}
					j++;
					partial_mask >>= 1;
				}

				if( group_search( positives, results, final_result + n ) ) {
					//cout<<"Găsit solutia"<<endl;
					results.insert( results.end(), temp_neg.begin(), temp_neg.end() );
					/*
					for( int i = 0; i< results.size(); i++ )
					{
						cout<<"results number list["<<i<<"]: "<<results.at(i)<<endl;
					}
					*/
					found = true;
					break;
				} else {
					cout<<"Still looking for..."<<endl;
					results.clear();
					temp_neg.clear();
				}
			}
		}

	}

	/*cout<<"hmmm!"<<endl;*/
}

bool SearchSolution::group_search( vector<long long int>& list, vector<long long int>& results, long long int result )
{
	vector<long long int> result_list;
	long long int temp = 0;
	for( int i=0; i<list.size(); i++ ) {
		//cout<<i<<": "<<list.at(i);
		if( temp+list.at(i) <= result ) {
			result_list.push_back( list.at(i));
			//cout<<" adaug la lista"<<endl;
			temp += list.at(i);
		} else {
			//cout<<" nu adaug la lista"<<endl;
			continue;
		}

	}

	//cout<<temp<<": "<<result<<endl;
	if( temp == result ) {
		results.swap(result_list);
		return true;
	}

	return false;
}
