#ifndef SEARCHSOLUTION_H
#define SEARCHSOLUTION_H
#include <vector>
using namespace std;

class SearchSolution {
private:
	typedef enum {
		ONE_SOLUTION,
		MULTI_SOLUTIONS
	} sel_solution;

	typedef enum {
		SECVENTIAL,
		ADVANCED
	} algorithm;

	typedef enum {
		ROW, MIX

	} method;

	/// switch to stop when found a solution
	sel_solution sel;

	/// algorithm used to find the solution
	algorithm alg;

	/// The number set from where wil bw taken the subset
	vector<long long int> numbers;

	/// The resulted list
	vector<long long int> results;

	/// Switch vector list results
	//vector<unsigned long long> xes;

	// Required sum value
	long long final_result;

	/// working switch variable
	//unsigned long long x;

	/// Found flag
	bool found;

	/// Search Algorithms
	void row_solution();
	void mix_solution();

	/// Searching Group
	bool group_search( vector<long long int>&, vector<long long int>&,  long long int );

public:
	SearchSolution();
	~SearchSolution();

	void set_numbers( vector<long long int>& );
	vector<long long int> get_results();
	void set_result( long long int );
	void get_solution();

};

#endif // SEARCHSOLUTION_H
