#ifndef CONVERTNUMBERS_H
#define CONVERTNUMBERS_H
#include <string>

using namespace std;

class ConvertNumbers {

public:
	ConvertNumbers();
	~ConvertNumbers();

	string to_int( string );
	string to_float( string );
};

#endif // CONVERTNUMBERS_H
