#include "convert_numbers.h"
#include <string>
#include <tr1/regex>
#include <iostream>

using namespace std;

ConvertNumbers::ConvertNumbers()
{
}

ConvertNumbers::~ConvertNumbers()
{
}

std::string ConvertNumbers::to_int( string str)
{
	string temp;
	//cout<<"push: "<<str<<endl;
	for(int i=0; i<str.size(); i++ ) {
		if(str.at(i) == '.' || str.at(i) == ',' || str.at(i) == '$' )
			continue;
		//cout<<"push: "<<str.at(i)<<endl;
		temp.push_back(str.at(i));
	}
	//cout<<"done"<<endl;
	return temp;
}

string ConvertNumbers::to_float( string str)
{
	//cout<<"str: "<<str<<endl;
	str.insert(str.end()-2, '.' );
	return str;
}

