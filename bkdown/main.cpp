#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <sstream>

using namespace std;

//#include "bkdownUI.h"
#include "search_solution.h"
#include "convert_numbers.h"

/**
 * @brief
 * @param argc
 * @param argv
 * @return
 */
int graphic( int, char ** );

template <class T>
bool numeric_cast(T& t, const std::string& s)
{
  std::istringstream iss(s);
  return !(iss >> std::dec >> t).fail();
}

int main(int argc, char **argv)
{
	char output[100];
	long long int value;
	vector<long long int>numar;
	vector<long long int>result;
	string str;

	SearchSolution s;

	ConvertNumbers c;

	if( argc == 4 ) {
		ifstream myReadFile;
		myReadFile.open( argv[1] );
		if (myReadFile.is_open()) {
			while (!myReadFile.eof()) {
				myReadFile >> output;
				stringstream buffer(output);
				str = c.to_int(output);
				if ( !numeric_cast<long long>(value, str) ) value = 0;
				numar.push_back(value);
			}
		}
		myReadFile.close();
		s.set_numbers( numar );
		str = c.to_int( argv[3]);
		//cout<<"..."<<str<<endl;
		if ( !numeric_cast<long long>(value, str) ) value = 0;
		//cout<<"value is: "<<value<<endl;
		s.set_result( value );
		s.get_solution();
		result = s.get_results();

		if( !result.empty() ) {
			cout<<"Solution found, please look on "<<argv[2];
			cout<<" file for the requested numbers"<<endl;
			ofstream outf(argv[2]);
			for(long i=0; i<result.size(); i++) {
				stringstream buff;
				buff<<result[i];
				str=buff.str();
				outf << c.to_float(str)<<endl;
			}
			outf.close();

			for(int i=0; i<result.size(); i++ ) {
				stringstream buff;
				buff<<result[i];
				str=buff.str();
				cout<<"nr: "<<c.to_float(str)<<endl;
			}

		}
		else
			cout<<"\n\tNo solution found"<<endl;
	}
	else if( argc>1 )
	{
		cout<<"how to use:\n bkdown <input_file> <output_file> <the_sum>\n";
		cout<<"Where:\n <input_file>\n\t the file with the numbers\n";
		cout<<"\t from where will get the solution.\n\t PLEASE, DO NOT LET FREE ROWS\n";
		cout<<"\t LAST ROW SHOULD BE THE LAST NUMBER FROM THE LIST AND NOT THE NEW LINE.\n";
		cout<<" <output_file>\n\t the file where will be written the numbers with which\n";
		cout<<"\t will give the solution.\n";
		cout<<" <the_sum>\n\t the sum you wish to have.\n";
		cout<<"\t DO NOT USE NUMBERS WITH DOLLAR ($) SIGN.\n";
		cout<<"Example:\n\t bkdown numbers_list.txt result.txt 4.00\n";
	}

	//SearchSolution s;
	//s.get_solution();
	return 0;
}
/*
int graphic( int argc, char **argv )
{
	bkdownUI* o = new bkdownUI();
	Fl_Double_Window* winc = o->make_window();
	winc->show(argc, argv);

	return Fl::run();
}
*/
